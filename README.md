# Lyric Check

## Team 7 10am

| Name | EID | GitLab ID |
| --- | --- | --- |
| Hassan Khan | hak658 | hakhan0301 |
| Stacy Jong | svj284 | stacyvjong |
| Jack Si | jks3395 | jacksi1 |
| Kenji Nakachi | kbn365 | kenjinakachi |
| Christopher Carrasco | cc66496 | guymanpersonboy |

## Git SHA

dbf2cc3b69895ddeb9913e0368e6a654ae908f39

## Project Leader

### Responsibilities

- Schedule meetings.
- Share screen or host VSCode Live Share.
- Monitor website health.
- Review merge requests.

### Phase Leader

- Phase 1: Hassan Khan
- Phase 2: Christopher Carrasco
- Phase 3: Jack Si
- Phase 4: Stacy Jong

## GitLab Pipelines

https://gitlab.com/kenjinakachi/lyriccheck/-/pipelines

## Website Link

https://www.lyriccheck.me

## Presentation

https://www.youtube.com/embed/ivWbISvDfsA

## Estimated Completion Time for Each Member

### Phase 1

- Hassan Khan: 10 hours
- Stacy Jong: 12 hours
- Jack Si: 12 hours
- Kenji Nakachi: 10 hours
- Christopher Carrasco: 15 hours

### Phase 2

- Hassan Khan: 16 hours
- Stacy Jong: 18 hours
- Jack Si: 20 hours
- Kenji Nakachi: 15 hours
- Christopher Carrasco: 18 hours

### Phase 3

- Hassan Khan: 12 hours
- Stacy Jong: 12 hours
- Jack Si: 12 hours
- Kenji Nakachi: 12 hours
- Christopher Carrasco: 12 hours

### Phase 4

- Hassan Khan: 6 hours
- Stacy Jong: 6 hours
- Jack Si: 6 hours
- Kenji Nakachi: 6 hours
- Christopher Carrasco: 6 hours

## Actual Completion Time for Each Member

### Phase 1

- Hassan Khan: 16 hours
- Stacy Jong: 15 hours
- Jack Si: 16 hours
- Kenji Nakachi: 15 hours
- Christopher Carrasco: 14 hours

### Phase 2

- Hassan Khan: 20 hours
- Stacy Jong: 21 hours
- Jack Si: 21 hours
- Kenji Nakachi: 20 hours
- Christopher Carrasco: 20 hours

### Phase 3

- Hassan Khan: 15 hours
- Stacy Jong: 14 hours
- Jack Si: 15 hours
- Kenji Nakachi: 13 hours
- Christopher Carrasco: 15 hours

### Phase 4

- Hassan Khan: 10 hours
- Stacy Jong: 9 hours
- Jack Si: 9 hours
- Kenji Nakachi: 9 hours
- Christopher Carrasco: 9 hours

## Comments

- Our presentation is located on the splash page on the last slide of the Carousel.

## References

- The setup for the About page was partially inspired by the team WeLikeSportz. Their codebase is linked here: https://gitlab.com/debbiew27/WeLikeSportz/
- The backend EB setup referenced the following guide: https://github.com/forbesye/cs373/blob/main/Flask_AWS_Deploy.md, therefore our backend/Dockerfile, backend/nginx.conf, backend/start.sh, and backend/uwsgi.ini files are all inspired by that resource.
- TexasVotes was referenced: https://gitlab.com/forbesye/fitsbits.
- Backend API and calling the backend api was referenced from: https://flask-restless-ng.readthedocs.io/en/latest/. Some code from the documentations was used.
- Material UI code was referenced from: https://mui.com/. Some code from the documentations was used.
- React Bootstrap code was referenced from: https://react-bootstrap.github.io/. Some code from the documentations was used.
- Jest Testing code was referenced from: https://jestjs.io/docs/api/. Some code from the documentations was used.
- Chai Testing code was referenced from: https://www.chaijs.com/api/. Some code from the documentations was used.
- SQL was referenced from: https://www.postgresql.org/docs/current. Some code from the documentations was used.
- Dockerfile code was referenced from https://docs.docker.com/engine/reference/builder/. Some code from the documentations was used.
- ESLint code was referenced from the ESLint documentation.
- API scraping code was referenced from their respective documentations. Some code from the documentations was used.
- npm/pip packages were referenced from their respective documentations. Some code from the documentations was used.
- Tools were referenced from their respective documentations. Some code from the documentations was used.
- A few lines of code were referenced from StackOverflow.
