/* eslint-disable */
const getAll = require('./get-all-models');


async function main() {
  const coaches = await getAll('coaches');

  const salaryWinrateCounts = {};


  coaches.forEach(({ salary, overall_wins, overall_losses }) => {
    const winrate = overall_wins / (overall_wins + overall_losses);

    if (!salaryWinrateCounts[salary]) {
      salaryWinrateCounts[salary] = { winrate: 0, count: 0 };
    }

    salaryWinrateCounts[salary].winrate += winrate;
    salaryWinrateCounts[salary].count++;
  });

  const salaryWinrates = Object.keys(salaryWinrateCounts).map(salary => ({
    salary,
    winrate: salaryWinrateCounts[salary].winrate / salaryWinrateCounts[salary].count
  }))


  console.log(JSON.stringify(salaryWinrates, null, 2));
}

main();
