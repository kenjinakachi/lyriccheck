/* eslint-disable */
const getAll = require('./get-all-models');


async function main() {
  const players = await getAll('players');

  const stateHeightSums = {};

  players.forEach(({ home_state, height }) => {
    const state = home_state?.replace(/[.]/g, '');

    if (!stateHeightSums[state]) {
      stateHeightSums[state] = { count: 0, val: 0 };
    }

    stateHeightSums[state].val += height;
    stateHeightSums[state].count++;
  });

  const stateHeights = Object.keys(stateHeightSums).map(state => ({
    state,
    avgHeight: stateHeightSums[state].val / stateHeightSums[state].count
  }));

  console.log(JSON.stringify(stateHeights, null, 2));
}

main();
