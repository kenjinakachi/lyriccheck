// code from https://observablehq.com/@observablehq/plot
// and https://github.com/observablehq/plot-create-react-app-example
// and https://observablehq.com/@observablehq/plot-shorthand

import * as Plot from "@observablehq/plot";
import rawData from './salary-winrate.json';

import { useEffect, useRef } from 'react';

const data = rawData
  .map(d => ({ ...d, salary: parseInt(d.salary, 10) }))
  .sort((a, b) => a.salary - b.salary);
// const data = rawData.map(({ salary, winrate }) =>
//   [parseInt(salary, 10), winrate]
// );

export default function Chart({
  width, height
}) {
  const ref = useRef();

  useEffect(() => {
    const chart = Plot.line(data, {
      x: 'salary', y: 'winrate'
    }).plot();
    ref.current.append(chart);
  }, [width, height]);

  return <div ref={ref} />;
};