import Container from "react-bootstrap/Container";

import TuitionWins from './visualizations/theirs/TuitionWins';
import StateHeights from './visualizations/theirs/StateHeights';
import SalaryWins from './visualizations/theirs/SalaryWins';

function TheirD3Visuals() {
  return (
    <div className='center' style={{ paddingTop: '25px' }}>
      <Container style={{ paddingBottom: '25px' }}>
        <h2>School's wins by tuition</h2>
        <TuitionWins width={750} />
      </Container>
      <Container style={{ paddingBottom: '25px' }}>
        <h2>States ranked by average height</h2>
        <StateHeights />
      </Container>

      <Container style={{ paddingBottom: '25px' }}>
        <h2>Coaches' salary by average winrate</h2>
        <SalaryWins width={700} />
      </Container>
    </div>
  );
}

export default TheirD3Visuals;